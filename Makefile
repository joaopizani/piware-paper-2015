
DIRGUARD=@mkdir -p $(@D)

MAIN_NAME=main
REFERENCES_NAME=references
LATEXMKOPTS=-pdf -f -quiet -pdflatex='xelatex -shell-escape'
TEXT_PARTS_SRC_ROOT=text
TEXT_PARTS_SRC_GENTEX=$(TEXT_PARTS_SRC_ROOT)

MAINTEX=$(TEXT_PARTS_SRC_ROOT)/$(MAIN_NAME).tex
MAINPDF=./$(MAIN_NAME).pdf
REFERENCES=$(REFERENCES_NAME).bib
TEXT_PARTS=section-introduction \
           section-overview \
           section-syntax \
           section-semantics \
           section-verification \
           section-casestudy \
           section-discussion \
           section-conclusion


CODE_AGDA_ROOT=code/agda
CODE_AGDA_SRC=$(CODE_AGDA_ROOT)/src
CODE_AGDA_GENTEX=$(CODE_AGDA_ROOT)/latex
CODE_AGDA_STY=texmf/tex/latex/agda.sty
CODE_AGDA_STDLIB=${HOME}/build/agda/lib/current/src

CODE_AGDA_MODULES=Basic \
                  Data/Finite/Base \
                  Data/Finite/Bool \
                  Data/FiniteNonEmpty/Base \
                  Data/FiniteNonEmpty/Bool \
                  Data/CausalStream \
                  PiWare/Atom \
                  PiWare/Atom/Bool \
                  PiWare/Gates \
                  PiWare/Gates/Nand \
                  PiWare/Gates/BoolTrio \
                  PiWare/Plugs/Core \
                  PiWare/Testing \
                  PiWare/Synthesizable \
                  PiWare/Circuit \
                  PiWare/Circuit/Typed \
                  PiWare/Circuit/Algebra \
                  PiWare/Simulation \
                  PiWare/Simulation/Equality \
                  PiWare/Simulation/Properties \
                  PiWare/Samples/BoolTrioComb \
                  PiWare/Samples/Muxes \
                  PiWare/Samples/BoolTrioSeq \
                  PiWare/ProofSamples/BoolTrioComb \
                  PiWare/ProofSamples/Muxes \
                  PiWare/Patterns \
                  PiWare/Patterns/Typed


all: $(MAINPDF)

$(MAINPDF): \
	$(REFERENCES) $(MAINTEX) \
	$(TEXT_PARTS:%=$(TEXT_PARTS_SRC_GENTEX)/%.tex) \
	$(CODE_AGDA_STY) \
	$(CODE_AGDA_MODULES:%=$(CODE_AGDA_GENTEX)/%.tex)
	TEXMFHOME=./texmf:$${TEXMFHOME} latexmk $(LATEXMKOPTS) $(MAINTEX)

$(CODE_AGDA_GENTEX)/%.tex: $(CODE_AGDA_SRC)/%.lagda
	$(DIRGUARD); agda --allow-unsolved-metas -i $(CODE_AGDA_SRC) --latex-dir=$(CODE_AGDA_GENTEX) --latex $<
	if [ -f $(subst /latex/,/patches/,$(@:.tex=.patch)) ]; then patch -d $(CODE_AGDA_ROOT) -p0 < $(subst /latex/,/patches/,$(@:.tex=.patch)); fi


clean:
	latexmk -c $(MAINTEX)

veryclean: clean
	rm -f $(CODE_AGDA_MODULES:%=$(CODE_AGDA_GENTEX)/%.tex)
	find $(CODE_AGDA_GENTEX) -type d -empty -delete

.PHONY: clean veryclean all

