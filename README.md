Π-Ware-paper-2015
=================

Contents of the first paper about [Π-Ware](https://github.com/joaopizani/piware).

The textual contents of the paper are in the `text` subdirectory, and all the code in the `code` directory.
More specifically, all Agda modules to be used are inside `code/agda`.
Even though only **some** pieces of that code might be included in the paper text itself,
complete Agda modules are under `code/agda`, as the Agda LaTeX backend requires type-correct programs.

The (not included) requirements for compiling the paper are:

 * LaTeX package `catchfilebetweentags`, probably already installed in your TeX distribution.

 * XITS package of fonts: can be found in the `texlive-fonts-extra` package or
   in the font's [official repository](https://github.com/khaledhosny/xits-math).

 * To generate the Agda `.tex` files from the the `.lagda` source code, you will need Agda 2.4.2.2
   with standard library 0.9.


The included `Makefile` can be used to perform the `.md → .tex` and `.tex → .pdf` compilations,
as well as all other dependencies (building necessary Agda modules, etc.).

There a git submodule setup under `code/agda/piware-src`.
This submodule tracks by default a branch of the canonical piware-agda repository called `paper-2015`.
To be able to use the agda code from Π-Ware in the paper, you need to initialize the submodule:

```bash
git submodule update --init --remote
```

And that should do the trick. To build: `CODE_AGDA_STDLIB='path_here' make`

