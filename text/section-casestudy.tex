\section{Case study: parallel prefix circuits}
\label{sec:casestudy}

In order to put in practice the definitions of Π-Ware we decided to perform a case study
involving \emph{parallel prefix circuits}.
Parallel prefix circuits are a wide family of circuit architectures that compute \emph{scans},
that is, given a binary operator $⊕$ and a vector of inputs $[x_0, x_1, x_2, ... , x_n]$, it will calculate
the output vector $[x_0, (x_0 ⊕ x_1), (x_0 ⊕ x_1 ⊕ x_2), ... , (x_0 ⊕ ... ⊕ x_n)]$.

When talking about parallel prefix circuits, we always assume that the binary operator $⊕$ is
\emph{associative}, thus allowing different parts of the output to be calculated \emph{in parallel}.
In Figure~\ref{fig:scan-simple} we show an example of a circuit utilizing maximal parallelism
to calculate a scan with 8 inputs.

\begin{figure}[ht]
  \centering{\includegraphics[width=0.28\textwidth]{imgs/scan-simple.pdf}}
  \caption{Example of an 8-input parallel prefix circuit.\label{fig:scan-simple}}
\end{figure}

In this style of diagram, the data flows from top to bottom, each black dot is a forking point
for wires and each white circle is an occurrence of the binary operator.
Our case study was heavily influenced by the paper ``An Algebra of Scans''~\cite{hinze}.
In this paper, the author defines a set of primitives and combinators from which any scan circuit
can be built, then states and proves algebraic properties of these combinators. 

Our work consisted of formalizing the same primitives and combinators using Π-Ware,
and proving the same basic algebraic facts about these combinators.
Also, we formalized what exactly means to be a scan circuit,
and proved that applying \emph{scan combinators} to scan circuits will result in a scan.

Several of the primitives and combinators defined in the original paper~\cite{hinze} match
exactly those present in Π-Ware, amongst them sequential (\AI{\_⟫\_}) and parallel composition (\AI{\_∥\_}) along
with the identity plug (\AF{id⤨}).
This coincidence makes the case study especially fruitful, as several of the basic algebraic properties
assumed in the original paper could be proved in Π-Ware.
For example, sequential combination (\AI{\_⟫\_}) forms a monoid of circuits, with \AF{id⤨} as identity:

\begin{center}
    \ExecuteMetaData[code/agda/latex/PiWare/Simulation/Properties.tex]{seq-left-identity}
    \newline

    \ExecuteMetaData[code/agda/latex/PiWare/Simulation/Properties.tex]{seq-assoc}
\end{center}

In fact, the need to state and prove these basic algebraic laws was what led us
to develop the notion of circuit equivalence (Section~\ref{subsec:circuit-equivalence}) in the first place.
We predict that such algebraic structures over circuits will be important when reasoning about
circuit transformations and synthesis.
For example, the identity laws for \AF{id⤨} allow us to \emph{remove} such plugs from any circuit,
while being \emph{certain} that the functional behaviour will not change.

The concept of scan circuit itself was formalized by defining a ``prototype'' scan,
which was assumed to be correct.
This definition is very inefficient (in terms of gate usage and also in depth),
but has a very simple inductive definition:

\begin{center}
\begin{code}
\>\AF{scan} \AY{:} \AF{∀} \AB{n} \AY{→} \AD{ℂ} \AB{n} \AB{n}\<%
\\
\>\AF{scan} \AI{zero} \AY{=} \<[12]%
\>[12]\AF{id⤨₀}\<%
\\
\>\AF{scan} \AY{(}\AI{suc} \AB{n}\AY{)} \AY{=} \<[12]%
\>[12]\AF{id⤨₁} \AI{∥} \AF{scan} \AB{n} \AI{⟫} \AF{fan} \AY{(}\AI{suc} \AB{n}\AY{)}\<%
\\
\end{code}
\end{center}

Besides the parts already mentioned (\AI{\_⟫\_}, \AI{\_∥\_}, \AF{id⤨}), here we also use the \AF{fan} primitive.
A term ``\AF{fan} \AB{n}'' has type \AD{ℂ} \AB{n} \AB{n}, and calculates $[x_0, (x_0 ⊕ x_1), (x_0 ⊕ x_2), ..., (x_0 ⊕ x_n)]$.
The diagram in Figure~\ref{fig:scan-inductive} illustrates the structure of ``\AF{scan} \AN{3}''.

\begin{figure}[ht]
    \centering{\includegraphics[width=0.25\textwidth]{imgs/scan-inductive.pdf}}
    \caption{Structure of the prototype scan of size 3.\label{fig:scan-inductive}}
\end{figure}

Using this specification, we could prove that several different architectures all indeed compute a scan.
The proofs rely on the fact that all of these architectures are built
by combining smaller scans into bigger ones.

Namely, we defined in Π-Ware the \emph{sequential scan combinator} (called \AF{\_▱\_}
and the \emph{parallel scan combinator} (called \AF{\_▯\_}).
Then we proved that the definition of these combinators indeed satisfies their namesake property:
whenever given scans as arguments, they produce a scan as output:

\begin{center}
\begin{code}
\>\AF{▱-law} \<[6]%
\>[6]\AY{:} \AF{scan} \AY{(}\AI{suc} \AB{m}\AY{)} \<[28]%
\>[28]\AF{▱} \<[30]%
\>[30]\AF{scan} \AY{(}\AI{suc} \AB{n}\AY{)} \<[43]%
\>[43]\AD{≋} \AF{scan} \AY{(}\AB{m} \AO{+} \AI{suc} \AB{n}\AY{)}\<%
\\
\>\AF{▯-law} \<[6]%
\>[6]\AY{:} \AF{scan} \AY{(}\AI{suc} \AB{m}\AY{)} \<[28]%
\>[28]\AF{▯} \<[30]%
\>[30]\AF{scan} \AB{n} \<[43]%
\>[43]\AD{≋} \AF{scan} \AY{(}\AI{suc} \AB{m} \AO{+} \AB{n}\AY{)}\<%
\end{code}
\end{center}

As an example of a proof involving lots of these algebraic properties,
we show the correctness of a \emph{serial scan}.
A serial scan is a parallel prefix circuit of maximal depth, as it makes
no use of parallelism at all, and it has the structure shown in Figure~\ref{fig:scan-serial}.

\begin{figure}[ht]
    \centering{\includegraphics[width=0.18\textwidth]{imgs/scan-serial.pdf}}
    \caption{Structure of a serial scan.\label{fig:scan-serial}}
\end{figure}

The Π-Ware description for \AF{serial} is pretty simple and makes essential use
of the parallel scan combinator (\AF{\_▯\_}).
The code for \AF{serial} is shown in Listing~\ref{lst:serial}

\begin{listing}[ht]
  \centering{\ExecuteMetaData[code/agda/latex/PiWare/ProofSamples/BoolTrioComb.tex]{serial}}
  \caption{Π-Ware description of a serial scan.\label{lst:serial}}
\end{listing}

Finally, we can then prove that \AF{serial} does indeed compute a scan.
The proof (Listing~\ref{lst:serial-is-scan}) relies on the key fact that \AF{\_▯\_} preserves scans.
Furthermore, it also relies on the fact that \AF{\_▯\_} is a congruence with regards to circuit equivalence,
and that two calls of \AF{scan} with equal arguments will be equivalent (\AF{scan-cong}).

\begin{listing}[ht]
    \centering{\ExecuteMetaData[code/agda/latex/PiWare/ProofSamples/BoolTrioComb.tex]{serial-is-scan}}
    \caption{Proof that \texttt{serial} computes a scan.\label{lst:serial-is-scan}}.
\end{listing}
