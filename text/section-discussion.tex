\section{Discussion}
\label{sec:discussion}

\subsection*{Related work}
\label{subsec:related-work}


There are numerous languages for hardware description; there is a wide
variety of techniques that may be used for hardware verification,
including the usage of automatic theorem provers, SAT solvers, model
checking, and interactive theorem provers, notably
HOL~\cite{camilleri1986hardware}. Systems such as ACL2 have been used
to prove correctness of entire microprocessors~\cite{hunt}. Rather
than attempt to survey these fields here, we will restrict ourselves
to the most closely related work.

There has been a great deal of work in the last thirty years marrying
functional programming and hardware design, leading to languages such
as Lava~\cite{lava}, Hawk~\cite{hawk} and ForSyDe~\cite{forsyde};
Sheeran~\cite{sheeran} gives an excellent overview.  None of these,
however, combine all of type safety, inductive proofs, and an executable
semantics.

The idea of using dependent types to describe circuits is not new and
can be traced back as far as Hanna~\cite{hanna},
with some more recent implementations (Coquet~\cite{coquet} and Fe-Si~\cite{fesi})
hosted in the Coq theorem prover.
Our design and implementation has been particularly inspired by Coquet.
Both Coquet and Π-Ware use a similar \emph{structural} description of
circuits, parametrized by the type of gates. The most important
difference between Π-Ware and Coquet, however, is that Π-Ware defines
a \emph{functional} semantics for circuits, while Coquet uses a
\emph{relational} semantics, i.e., the semantics are specified by
defining a suitably indexed inductive data type. The choice of
semantics style is crucial: Π-Ware circuits can be tested, simulated,
and verified as if they are any other Agda function. Where Coq's
richer language for proof tactics may provide a great deal of
automation, the functional semantics presented here reduce \emph{for free},
without relying on the invocation of tactics or proof search.

The paper ``Constructing Correct
Circuits''~\cite{brady-constructing} gives a clear example of how
dependent types can \emph{tie together} specification and
implementation.  In this paper, the authors give a mapping between
Peano naturals and binary numbers, then used to build a
(ripple-carry) binary adder which is \emph{correct by
  construction}.  The approach taken in Π-Ware is significantly
different.  Rather than carry the functional specification of a
circuit \emph{in its type}, we clearly separate the construction,
testing, and verification of circuits.  This means that, for
example, a designer can first simulate some instances of a design
and get confidence in its correctness before trying to
\emph{prove} it. This greater degree of freedom may be
particularly useful when exploring the design space, deferring the
testing and verification effort until a satisfactory candidate
design has been found.

\vspace{1cm}

\subsection*{Future work}
\label{subsec:future-work}

\paragraph{Equality plugs}

\acrodef{ITT}{Intensional Type Theory}
\acrodef{ETT}{Extensional Type Theory}
When explaining the behaviour of \AI{Plug}s in Section~\ref{sec:structure}, we said that they
perform no computation. But further than that, some plugs in fact have also \emph{no structural effect}.
By this we mean plugs whose mapping is the identity function.
They usually are an expression of arithmetic equalities over circuit indices, such as associativity:

\begin{center}
    \ExecuteMetaData[code/agda/latex/PiWare/Plugs/Core.tex]{assoc-fin}
\end{center}

The need to place such a \AI{Plug} between two circuits is essentially an artifact of \ac{ITT}.
In the sequence constructor (\AI{\_⟫\_}), the output index of the first parameter and input index of the second
must be \emph{definitionally} equal, that is, they must have the same normal form.
If Agda had the \emph{equality reflection rule},
then equalities involving indices could be used during type checking,
and we would not need to insert \emph{equality plugs}.

Right now we are investigating two approaches to make this issue less inconvenient.
Firstly, we can use the \emph{ring solver} from Agda's standard library (coupled with reflection)
to automatically solve index equalities and introduce the corresponding plugs whenever needed.
Secondly, there was a recent addition to Agda of a language pragma called \texttt{REWRITE},
which allows for user-defined equalities to be added to Agda's typing rules,
essentially turning Agda into an \ac{ETT}.
We will investigate how the use of this pragma affects our library and examples.

\paragraph{Functional language}

Even though we are using a functional language to model our circuits, the
circuit description themselves are very low-level. In particular, we
need to rewire intermediate results explicitly using our \AI{Plug}
constructors. While our style closely resembles the netlist
representation of circuits, we would like to provide circuit designers
with a more high-level, applicative interface.

One problem that we must address to do so, however, is that of
observable sharing~\cite{obs-sharing}. Any domain specific
language for hardware description embedded in a general purpose
functional language must, at some point, ensure that the sharing and
recursion of the circuit definitions are not lost. Although various
solutions do exist, these typically place a higher burden on the
programmer through the necessity of explicit fixed-point and sharing
combinators or rely on specific compiler support. We hope to find a
satisfactory solution to this problem in the context of dependently
typed programming languages such as Agda, and use this to define a
more ``functional'' layer on top of the definitions presented here.

\paragraph{Typed circuits}

While Π-Ware rules out certain errors, such as short-circuits, we
would like to investigate how to provide stronger static
guarantees. So far, we have parametrized the type of circuits by the
\emph{size} of their inputs and outputs; we have started investigating
how to parameterize circuits by their \emph{type}.

For example, the type of a 2-input multiplexer would then become
``\AD{ℂ} \AY{(}\AD{Bool} \AD{×} \AY{(}\AD{Bool} \AD{×} \AD{Bool}\AY{)}\AY{)} \AD{Bool}'',
rather than the less informative ``\AD{ℂ} \AN{3} \AN{1}''.
To add extra type information to our circuits, we define a \AK{record}
wrapper for typed circuits (Listing~\ref{lst:circuit-typed}).

\begin{listing}
    \centering{\ExecuteMetaData[code/agda/latex/PiWare/Circuit/Typed.tex]{Circuit-typed}}
    \caption{Typed Circuit type.\label{lst:circuit-typed}}
\end{listing}

Here we require that the input and output types of our circuits are
synthesizable -- that is, they can indeed be represented in our simulation
semantics (as vectors of atoms). By adding a series of \emph{smart constructors} that
produce and combine such typed circuits, we can provide a more convenient and type-safe
interface to our library. We are currently extending our library with
such type-safe definitions, including the use of reflection to generate the
required serializer/deserializer and proof that they are inverses.



% Wouter:
%\paragraph{Architectural modelling}
% Should we speculate about architectural modelling and relating
% different level of abstraction/refinement here? Only if there is
% space -- if so, we should certainly cite the Panagiotis paper on
% verification of pipelined architectures.
