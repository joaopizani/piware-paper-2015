\section{Introduction}
\label{sec:introduction}

\acrodef{RTL}{Register-Transfer Level}
\acrodef{DSL}{Domain-Specific Language}
\acrodef{ILP}{Instruction-Level Parallelism}
\acrodef{HDL}{Hardware Description Language}

There is a long tradition of using functional programming to model hardware circuits.
Dating as far back as the 1980's, there have been many different proposed \acp{DSL}
for the design and verification of circuits~\cite{mufp,lava,forsyde,coquet}.
Initially these languages were mostly standalone, but later
\emph{embedded} \acp{DSL} for hardware were developed, hosted in functional languages such as Haskell or ML.

All of these \emph{functional hardware} \acp{DSL} have some limitations
with respects to \emph{(type) safety} or aspects of the design process that they support.
Most of them allow for simulation, some support synthesis to \ac{RTL} netlists; others are focused on verification
(using a combination of SMT solvers, automated theorem provers, or interactive proof assistants).
We would argue, however, that none of these are \emph{unified} typed languages for the design,
simulation, verification, and synthesis of hardware circuits.

Yet the need for better tools for the design of custom hardware accelerators is greater than ever.
The performance of general purpose processors is becoming increasingly harder to improve,
as we have already long ago faced the power wall and \ac{ILP} shows diminishing returns~\cite{dark-silicon}.
There is a growing demand for hardware acceleration that is both easy to maintain and accurate.

This paper presents Π-Ware, a \ac{HDL} embedded in \emph{Agda}~\cite{oury-power,norell:thesis}, a dependently-typed programming language.
Π-Ware provides a single, strongly-typed language which supports the definition,
synthesis, simulation, testing and formal verification of complex circuits.
After giving a high-level overview of the language and its features (Section~\ref{sec:overview}),
this paper makes the following contributions:

\begin{itemize}
    \item Π-Ware has been designed to be a safe and strongly-typed language.
        Our embedding (Section~\ref{sec:structure}) makes use of dependent types to provide
        safety guarantees beyond the ones offered by \acp{HDL} embedded in simply-typed functional languages.
        The embedding is parameterized by the type of fundamental data over the wires
        and the library of fundamental gates being used.
        For example, instead of using only logic gates,
        one could add binary arithmetic operators to the fundamental library.
        This raises the level of abstraction of the description and simplifies verification.

    \item Unlike other embeddings in proof assistants (such as Coquet~\cite{coquet}),
        Π-Ware circuits are executable.
        We defined functional semantics for both combinational and sequential circuits (Section~\ref{sec:semantics}).
        This may seem trivial, but providing a total semantics of circuits that run forever requires some care.
        Specifically, we define a \emph{causal stream-based} semantics to model the simulation of sequential circuits.

      \item
        As the circuit semantics is executable, we can use it to test our designs.
        Furthermore, for finite domains,
        we can automatically derive a proof by exhaustive testing (Section~\ref{sec:verification}).
        More generally, the full power of Agda as an interactive theorem prover can be used
        to prove properties of \emph{circuit generators}.
        These generators are usually defined using some sort of recursive pattern (\emph{connection pattern}),
        and proofs about them rely on induction following the same pattern (\emph{proof combinator}).

    \item Finally, we show how all these features may be combined in a single case study:
        the verification of parallel prefix circuits (Section~\ref{sec:casestudy}).
        In Π-Ware, such circuits may be described, tested, and
        verified -- in a fashion that was previously only possible on paper~\cite{hinze}.
\end{itemize}

