\section{Overview}
\label{sec:overview}

We can best illustrate circuit models in Π-Ware by analyzing a relatively simple example.
Let us model a 2-way multiplexer (\AF{mux}).
For convenience, we consider that booleans are being carried over the wires,
and that we have the usual set of fundamental gates at our disposal:
\(\{\text{NOT}, \text{AND}, \text{OR}\}\).

A reasonable first step when designing a circuit is to think of its \emph{specification}.
For such a simple and small circuit as \AF{mux}, a \emph{truth table} defines it concisely enough.
Our \AF{mux} has two data inputs (\texttt{A} and \texttt{B}), and also a selection input (\texttt{S}).
The mux should behave in such a way that, whenever (\(S = 0\)),
the output (\texttt{Z}) should be equal to input \texttt{A}, otherwise the output should be equal to input \texttt{B}.
This is expressed in Table~\ref{tab:mux}.

\begin{table}
    \begin{center}
    \begin{tabular}{cccc}
        \toprule
        S & A & B & Z \\
        \midrule
        0  &  0  &  0  &  0 \\
        0  &  0  &  1  &  0 \\
        0  &  1  &  0  &  1 \\
        0  &  1  &  1  &  1 \\
        1  &  0  &  0  &  0 \\
        1  &  0  &  1  &  1 \\
        1  &  1  &  0  &  0 \\
        1  &  1  &  1  &  1 \\
        \bottomrule
    \end{tabular}
    \end{center}
    \caption{Truth table specification of \texttt{mux}.\label{tab:mux}}
\end{table}

From the truth table we can (straightforwardly) derive a boolean formula:

\begin{equation}
Z = (A ∧ ¬S) ∨ (B ∧ S)
\end{equation}

From this logical formula, a designer could then \emph{implement} a circuit
with the structure shown in Figure~\ref{fig:mux-wireframe}.
This kind of graphical model is often known as \emph{block diagram}.

\begin{figure}[ht]
    \centering{\includegraphics[width=0.45\textwidth]{imgs/mux.pdf}}
    \caption{Wireframe diagram of \texttt{mux}.\label{fig:mux-wireframe}}
\end{figure}

We can also view such a diagram in a different way, by considering the fundamental gates present,
and grouping them using \emph{sequential} (\AI{\_⟫\_}) and \emph{parallel} (\AI{\_∥\_}) composition.
This corresponds exactly to the definition of \AF{mux} in Π-Ware, shown in Listing~\ref{lst:mux}.

\begin{listing}
    \centering{\ExecuteMetaData[code/agda/latex/PiWare/Samples/Muxes.tex]{mux}}
    \caption{Π-Ware model of \texttt{mux}.\label{lst:mux}}
\end{listing}

In this description, we use all three kinds of fundamental gates:
\texttt{AND} (\AF{∧ℂ}), \texttt{OR} (\AF{∨ℂ}) and \texttt{NOT} (\AF{¬ℂ}).
Notice how the circuit type (\AD{C} \AN{3} \AN{1}) is \emph{indexed} by the \emph{sizes} of the input and output.
The \emph{total} size of all inputs of \AF{mux} amounts to \AN{3} and total size of all outputs amounts to \AN{1}.
Besides the fundamental gates and composition, we also use some blocks to do \emph{rewiring}.
The circuit called \AF{fork×⤨} outputs two exact copies of its input bus side-by-side,
while \AF{fst⤨₁} and \AF{snd⤨₁} select respectively the first and second wire from an input of size \AN{2}.

The \AF{mux} example shows how Π-Ware circuits are described in a low level of abstraction.
Circuits are combined in an \emph{architectural} way,
and there is no way \emph{in the \ac{DSL}} to refer to intermediary results (no binding).
We discuss how this low-level description relates to other levels of abstraction in Section~\ref{sec:discussion}.

In the definition of \AF{mux}, the size parameters to the \AD{C} type constructor are constants, but they need not be in general.
Using dependent types, we can precisely define and reason about \emph{circuit generators}.
For example, in Listing~\ref{lst:muxN} we define \AF{muxN}, an inductively defined circuit generator.
A term ``\AF{muxN} \AB{n}'' takes two inputs of size \AB{n} and produces an output of the same size.
In this definition, \AF{nil⤨} stands for the \emph{empty} circuit
(has \AI{zero} outputs and does no computation),
while \AF{adapt⤨} does the necessary reordering of the wires (associativity and commutativity).

\begin{listing}[ht]
    \centering{\ExecuteMetaData[code/agda/latex/PiWare/Samples/Muxes.tex]{muxN}}
    \newline

    \ExecuteMetaData[code/agda/latex/PiWare/Samples/Muxes.tex]{adapt-type}
    \caption{Π-Ware model of the \texttt{muxN} generator.\label{lst:muxN}}
\end{listing}

Even though circuit semantics is only given in Section~\ref{sec:semantics},
we can already see what would be possible given a functional semantics for our examples.
For now, let's assume the semantic function for circuits has the following type:

\begin{center}
\AF{⟦\_⟧} \AY{:} \AD{ℂ} \AB{i} \AB{o} \AY{→} \AY{(}\AD{Vec} \AD{Bool} \AB{i} \AY{→} \AD{Vec} \AD{Bool} \AB{o}\AY{)}
\end{center}

That is, it takes a circuit with inputs of \emph{size} \AB{i} and outputs of \emph{size} \AB{o},
and returns its semantics: a function between appropriately-sized binary words.
One first possibility is to just \emph{test} \AF{mux} to gain confidence in its correctness. 
As dependent types may perform evaluation during type checking, we can formulate our tests
as a type checking problem, requiring that our circuit will compute the required type by definition:


\begin{center}
\begin{code}
\>\AF{test1} \AY{:} \AF{⟦} \AF{mux} \AF{⟧} \AY{(}\AI{false} \<[23]%
\>[23]\AI{∷} \AY{(}\AI{true} \AI{∷} \AI{false} \AI{∷} \AI{[]}\AY{)}\AY{)} \<[46]%
\>[46]\AD{≡} \AY{(}\AI{true} \<[54]%
\>[54]\AI{∷} \AI{[]}\AY{)}\<%
\\
\>\AF{test2} \AY{:} \AF{⟦} \AF{mux} \AF{⟧} \AY{(}\AI{true} \<[23]%
\>[23]\AI{∷} \AY{(}\AI{true} \AI{∷} \AI{false} \AI{∷} \AI{[]}\AY{)}\AY{)} \<[46]%
\>[46]\AD{≡} \AY{(}\AI{false} \<[54]%
\>[54]\AI{∷} \AI{[]}\AY{)}\<%
\end{code}
\end{center}

This approach works fine to check the correctness of the simple \AF{mux}:
just write one test for each line of the truth table.
However, we cannot write an \emph{infinite} number of tests,
so to definitely convince ourselves of the correctness of \AF{muxN}
we will want to \emph{prove} a more general statement, such as:

\begin{center}
\AF{muxN⊑ite} \AY{:} \AY{∀} \AB{n} \AY{→} \AF{⟦} \AF{muxN} \AB{n} \AF{⟧} \AY{(}\AB{s} \AI{∷} \AY{(}\AB{a} \AF{++} \AB{b}\AY{)}\AY{)} \AD{≡} \AF{if} \AB{s} \AF{then} \AB{b} \AF{else} \AB{a}
\end{center}

Here we can regard the \AF{if\_then\_else\_} function as a \emph{specification} for \AF{muxN},
and \AF{muxN⊑ite} as a \emph{proof} that \AF{muxN} complies with this specification.
This proof can be written by induction on \AB{n}.
The base case will be vacuously true (as there are no elements in \AD{Vec} \AD{Bool} \AI{zero}),
and the successor case will rely on a \emph{similar correctness proof of} \AF{mux},
along with the inductive hypothesis and auxiliary lemmas involving \AF{adapt⤨} (commutativity, associativity, etc.).

We discuss proofs of circuit (generator) properties more thoroughly in Section~\ref{sec:verification}.
In that section we also talk about a notion of \emph{equivalence} between circuits
and several algebraic properties of circuit constructors and \emph{combinators}.
As a prerequisite for verification, however,
we first must precisely define the syntax and semantics of circuits,
respectively in Sections~\ref{sec:structure} and \ref{sec:semantics}.

