\section{Circuit semantics}
\label{sec:semantics}

\acrodef{AST}{Abstract Syntax Tree}
Due to the choice of using deep embedding to implement our \ac{DSL},
it is possible to write several different semantics for circuit models.

When talking about deeply embedded languages, a semantic function is just a function
mapping the \ac{AST} of our \ac{DSL} to a desired \emph{carrier} type.
All of the circuit semantics currently implemented in Π-Ware are \emph{compositional},
which means that they can be defined by \emph{folding} the \AD{ℂ} type with an algebra.

The module \AM{PiWare.Circuit.Algebra} defines the \emph{algebra type} for \AD{ℂ} (as a \AK{record}),
along with the associated \emph{catamorphism} (fold).
There are two algebra types: one for combinational circuits (\ARR{ℂσA})
and one for (possibly) sequential ones (\ARR{ℂA}).
The only difference between them is that a case for \AI{DelayLoop} is absent from \ARR{ℂσA}.
Here we show the algebra type for combinational circuits (\ARR{ℂσA}):

\begin{center}
    \ExecuteMetaData[code/agda/latex/PiWare/Circuit/Algebra.tex]{Circuit-combinational-algebra-type}
\end{center}

We use Agda's feature of \emph{sections} (similar to Coq's sections)
to parameterize the catamorphism by an algebra instance and the algebra type itself by a \emph{carrier} (called \AB{T}).
Within the section, the definition and type of catamorphism become very simple and understandable.

Listing~\ref{lst:cata-circuit} shows the catamorphism for combinational circuits (\AF{cataℂσ}).
Notice how (due to the \AI{σ} index) there is no need to define a clause for \AI{DelayLoop}.

\begin{listing}
    \centering{\ExecuteMetaData[code/agda/latex/PiWare/Circuit/Algebra.tex]{Circuit-combinational-cata}}
    \caption{Catamorphism for combinational circuits.\label{lst:cata-circuit}}  
\end{listing}


\subsection{Combinational simulation}
\label{subsec:comb-simulation}

    As a particular example of such a compositional semantics,
    we defined \emph{executable} simulation for Π-Ware circuit models,
    which maps circuits to the domain of Agda functions.
    This semantics is \emph{executable} in the sense that,
    by applying the function obtained using the semantics to an input,
    the same output should be calculated as if the circuit had been implemented in hardware and run.

    When getting the simulation semantics of a combinational circuit,
    we want to obtain a function between appropriately-sized words, that is,
    a circuit of type ``\AD{ℂ} \AB{i} \AB{o}'' should result in a function of type ``\AF{W} \AB{i} \AY{→} \AF{W} \AB{o}''.
    Thus the carrier type for the combinational simulation algebra is:
    \begin{center}
        \ExecuteMetaData[code/agda/latex/PiWare/Gates.tex]{Word-function}
    \end{center}

    With the appropriate carrier defined,
    we get a very simple type and definition for the combinational simulation function:

    \begin{center}
        \ExecuteMetaData[code/agda/latex/PiWare/Simulation.tex]{simulation-combinational}
    \end{center}

    Notice how the type of the semantic function
    \emph{requires} the interpreted circuit to be combinational (it must be indexed by \AI{σ}).
    In this way, the algebra used (\AF{simulationσ}) does not have a field for the \AI{DelayLoop} case.
    We show on Listing~\ref{lst:circuit-simulation-defs} the definitions for each of the fields in
    the combinational simulation algebra.

    \begin{listing}
    \centering{\ExecuteMetaData[code/agda/latex/PiWare/Simulation.tex]{combinator-Word-function-defs}}
    \caption{Simulation semantics for combinational circuits.\label{lst:circuit-simulation-defs}}
    \end{listing}

    The cases for sequential (\AF{seqσ}) and parallel composition (\AF{parσ}) are self-evident,
    relying respectively on function composition and \AF{map} over products.
    In the case of fundamental gates, we simply rely on that gate's specification function.
    This leaves the most interesting definition: \AF{plugσ}.
    In the case of a \AI{Plug}, we build the output word \emph{pointwise},
    with each output index being passed to the function argument of \AF{tabulate}.
    First, we \AF{lookup} the output index in the plug mapping \AB{p}, obtaining the corresponding input index.
    Then we simply use this index to \AF{lookup} the input word and place the correct \AL{Atom} on the output.

    Let's now consider a simple example circuit, its simulation semantics and the involved types,
    to better understand how all these definitions fit into place.
    We consider a two-input \texttt{NAND} gate (\AF{⊼ℂ}), with the following type and definition:

    \begin{center}
        \ExecuteMetaData[code/agda/latex/PiWare/Samples/BoolTrioComb.tex]{nand}
    \end{center}

    The gate is described using two-input conjunction (\AF{∧ℂ}) and one-input negation (\AF{¬ℂ}) as building blocks,
    and these pieces come from the library of fundamental gates that we are using (\AM{PiWare.Gates.BoolTrio}).
    By evaluating \AF{⊼ℂ} we then obtain the following function:

    \begin{center}
    \begin{code}
    \>\AF{⟦} \AF{⊼ℂ} \AF{⟧} \AY{:} \AF{W} \AN{2} \AY{→} \AF{W} \AN{1}\<%
    \\
    \>\AF{⟦} \AF{⊼ℂ} \AF{⟧} \AY{=} \AL{spec} \AI{¬ℂ'} \AF{∘′} \AL{spec} \AI{∧ℂ'}\<%
    \end{code}
    \end{center}

    To simulate \AF{⊼ℂ} we rely on the specification functions of both gates and in function composition.
    The names \AI{¬ℂ'} and \AI{∧ℂ'} are just the gate \emph{identifiers}.
    If we reduce the expression above further (expanding the occurrences of \AL{spec}), we obtain the following:

    \begin{center}
    \begin{code}
    \>\AF{⟦} \AF{⊼ℂ} \AF{⟧} \AY{:} \AF{W} \AN{2} \AY{→} \AF{W} \AN{1}\<%
    \\
    \>\AF{⟦} \AF{⊼ℂ} \AF{⟧} \AY{=} λ \AY{\{} \AY{(}\AB{x} \AI{∷} \AB{y} \AI{∷} \AI{[]}\AY{)} \AY{→} \AF{[} \AF{not} \AY{(}\AB{x} \AF{∧} \AB{y}\AY{)} \AF{]} \AY{\}}\<%
    \end{code}
    \end{center}

    The verification of circuits and circuit generators will be discussed in detail in Section~\ref{sec:verification}.
    But it is already clear that it will rely heavily on laws involving vectors,
    as well as algebraic properties of the fundamental gates and plugs used in the design.


\subsection{Sequential simulation}
\label{subsec:seq-simulation}

    \emph{Sequential} circuits are those in which their output at any given instant may depend
    not only on a combination of the input at the same instant, but on the \emph{sequence}
    of previous inputs.

    In Π-Ware, we model only the \emph{discrete time domain}, and therefore a circuit's input
    \emph{signal}\footnote{By signal we mean a function on the time domain.}
    is \emph{piecewise constant} (as well as its output signal).
    Because of this characteristic, we can model both input and output signals as
    \AD{Stream}s\footnote{A stream is an infinite list, i.e., a list without the \AI{Nil} constructor.},
    in which each element of the \AD{Stream} is a word.

    Perhaps the simplest example of a circuit with internal state is \AF{shift}.
    This circuit will output at any clock cycle \AB{t} the value present on its input at the preceding cycle.
    The architecture of \AF{shift} consists simply of one \AI{DelayLoop} and one \AI{Plug}:

    \begin{center}
        \ExecuteMetaData[code/agda/latex/PiWare/Samples/BoolTrioSeq.tex]{shift}
    \end{center}

    For circuits which have the \AI{ω} (omega) tag in their type, 
    we need to use the \emph{sequential execution semantics} (\AF{⟦\_⟧ω}).
    In the specific case of \AF{shift}, the function obtained via the sequential
    simulation semantics will have the following type:

    \begin{center}
    \begin{code}
    \>\AF{⟦} \AF{shift} \AF{⟧ω} \AY{:} \AD{Stream} \AY{(}\AF{W} \AN{1}\AY{)} \AY{→} \AD{Stream} \AY{(}\AF{W} \AN{1}\AY{)}\<%
    \end{code}
    \end{center}

    The function obtained via \AF{⟦\_⟧ω} consumes and produces a \AD{Stream} of adequately-sized words.
    To explain in detail how the sequential semantics is actually defined, however,
    we have to mention a key distinction between digital circuits and stream functions in general:
    An unconstrained stream function (that is, an arbitrary element of type \AF{Stream} \AB{α} \AY{→} \AF{Stream} \AB{β})
    can (possibly) \emph{look into the future}.
    Considering \AD{Stream}s over the discrete time domain,
    one simple example of stream function that ``looks into the future'' is \AF{tail}.

    \begin{center}
        \ExecuteMetaData[code/agda/latex/Data/CausalStream.tex]{tail}
    \end{center}

    The element at position \AN{0} in the output of \AF{tail} depends on the input at position \AN{1}, and so forth.
    Sequential circuits clearly cannot show this behaviour (at least not if we want to physically implement them).
    As we want our sequential circuits to be synthesizable to actual hardware,
    we should ensure that our semantics will only ever produce \emph{causal stream functions}.

    One way to define a causal stream function is
    as the unfolding of a function producing only the \emph{next} output, given the current and past inputs.
    We call these functions \emph{causal step functions}, and they are defined as follows:

    \begin{center}
        \ExecuteMetaData[code/agda/latex/Data/CausalStream.tex]{causal-step}
    \end{center}

    The symbol \AF{Γc} means \emph{causal context}, and it is defined simply as a non-empty list (\AD{List⁺}),
    that is, a pair of the head (current value) with a possibly-empty tail (past values).

    \begin{center}
        \ExecuteMetaData[code/agda/latex/Data/CausalStream.tex]{causal-context}
    \end{center}

    Coming back to the definition of simulation semantics for sequential circuits,
    we can now establish the \emph{carrier} type for the algebra of sequential circuits
    as being a \emph{causal stream function} between words of the appropriate length.
    Then, the \emph{causal} simulation of a circuit is defined as a catamorphism over \AD{ℂ} with the \AF{simulationc} algebra:

    \begin{center}
        \ExecuteMetaData[code/agda/latex/PiWare/Simulation.tex]{Word-causal-function}
    \end{center}

    \begin{center}
        \ExecuteMetaData[code/agda/latex/PiWare/Simulation.tex]{simulation-causal}
    \end{center}

    The definitions of the fields of \AF{simulationc} make use of
    the fields in the combinational algebra (\AF{simulationσ}).
    The fields \AL{GateA} and \AL{PlugA} simply take the \emph{present} value from the causal context
    and pass it to the corresponding combinational field (\AF{gateσ} and \AF{plugσ}), thus we don't show them here.
    The interesting cases are \AI{DelayLoop}, as well as the composite constructors (\AI{\_⟫\_} and \AI{\_∥\_}):

    \begin{flushleft}
        \ExecuteMetaData[code/agda/latex/PiWare/Simulation.tex]{delay-causal-def}
        \ExecuteMetaData[code/agda/latex/PiWare/Simulation.tex]{combinator-causal-function-composite}
    \end{flushleft}

    The \AF{delay} function is responsible for taking the regular word function taking \AB{l} extra wires
    (\AB{l} stands for ``loop'') and transforming it into a causal step function
    (depending on the history of inputs instead of the current state).
    According to this semantics, a circuit built with \AI{DelayLoop} corresponds to a \emph{Mealy machine},
    where the state has size \AB{l}, and the combinational circuit inside of it
    calculates \emph{both} the next output and the next state.

    By calling our causal semantic function (\AF{⟦\_⟧c}) over a circuit we obtain a causal \emph{step function}.
    Then, by just unfolding this step function we obtain the \emph{causal stream function}
    which is actually the user-facing type for the simulation of sequential circuits:

    \begin{center}
        \ExecuteMetaData[code/agda/latex/PiWare/Simulation.tex]{simulation-sequential}
    \end{center}

    In contrast with the type of \AF{⟦\_⟧} (the combinational semantics), the type of \AF{⟦\_⟧ω}
    makes no requirement on how the circuit parameter should be \emph{indexed}.
    This means that the sequential semantics can be used to obtain a stream function
    from both sequential and combinational circuits.
    In the case of evaluating a combinational circuit using \AF{⟦\_⟧ω}, the obtained stream function
    just applies the calculation performed by the circuit \emph{pointwise} on the stream (ignoring the past).
