\section{Circuit structure}
\label{sec:structure}

The \emph{syntax} of Π-Ware models gives a low-level description of a circuit's \emph{architecture},
indicating how fundamental \emph{gates} are connected to each other to perform a certain task.
This style approximates \emph{block diagrams} usually drawn by hardware designers, but with a key distinction:
in Π-Ware, components are connected to each other in a \emph{nameless} fashion,
without explicitly naming ports or wires.

As Π-Ware is a \emph{deeply-embedded} \ac{DSL}, the syntax of the language is defined by a datatype (called \AD{ℂ}).
Our \ac{DSL} distinguishes between \emph{combinational} and \emph{sequential} circuits.
In summary, sequential circuits can be said to have \emph{internal state}, while combinational ones do not.
We denote this distinction by \emph{indexing} the \AD{ℂ} type with an element of \AD{IsComb}:

\begin{center}
    \ExecuteMetaData[code/agda/latex/PiWare/Circuit.tex]{IsComb}
\end{center}

By convention, we consider circuits indexed with the \AI{ω} (omega) value to be sequential.
The choice was slightly motivated by the usage of $\Sigma^{\omega}$ in mathematics to represent
the set of all infinite sequences over a given alphabet $Σ$.
With the combinational/sequential distinction clear, we present \AD{ℂ} in Listing~\ref{lst:circuit},
the core of the whole library.

\begin{listing}[ht]
    \ExecuteMetaData[code/agda/latex/PiWare/Circuit.tex]{Circuit-predecl}
    \centering{\ExecuteMetaData[code/agda/latex/PiWare/Circuit.tex]{Circuit}}
    \caption{The circuit datatype (\texttt{ℂ}).\label{lst:circuit}}
\end{listing}

The first thing to notice is that besides \AD{IsComb}, the circuit datatype (\AD{ℂ}) is also indexed
by two natural numbers.
These correspond, respectively, to the \emph{total} number of input wires into the circuit
and total number of output wires from the circuit.

To facilitate discussion of the constructors of \AD{ℂ},
we categorize them as either \emph{primitive} or \emph{composite}:
composite constructors take arguments of type \AD{ℂ}, while primitive ones do not.
First, we look at the primitive constructors:

\begin{itemize}
    \item Circuits constructed with \AI{Gate} are the smallest possible ones \emph{with computational content}.
        The whole \AM{PiWare.Circuit} module is parameterized by a \emph{gate library}
        (detailed in Section~\ref{subsec:gates}),
        and by calling \AI{Gate} we simply pick one of those gates to use as building block.

    \item The other primitive constructor is \AI{Plug},
        which is necessary due to the \emph{nameless} fashion in which we compose circuits.
        Since it is impossible to refer to any specific circuit port we cannot, for example,
        map the ``first'' output of a circuit to the ``second'' input of another.
        Plugs are required to do \emph{rewiring}, but they perform \emph{no computation}.
\end{itemize}

The argument to the \AI{Plug} constructor has type \AB{i} \AF{⤪} \AB{o},
and this is just a synonym for a (first-order representation of a)
function from output wires (indices) to input wires (indices).

\begin{center}
    \ExecuteMetaData[code/agda/latex/PiWare/Plugs/Core.tex]{Plug-type}
\end{center}

An intuitive definition for (\AB{i} \AF{⤪} \AB{o}) would be (\AD{Fin} \AB{o} \AY{→} \AD{Fin} \AB{i}),
but we opted for the \AD{Vec} representation in order to get easier combination of plugs and easier proofs.
Using such a mapping, no \AI{Plug} can ever be built
containing any information other than the origin of each output wire.

The composite constructors in Π-Ware represent ways in which smaller circuits can be connected to form a larger one.
First, let us focus on the most interesting of them: \AI{DelayLoop}.
Both other composite constructors (\AI{\_⟫\_} and \AI{\_∥\_}) \emph{preserve} the \AD{IsComb} index.
The \AI{DelayLoop} constructor, however, is an exception:
it is the only way to build a sequential circuit given a combinational one as argument.

This single possible way to \emph{introduce state} makes the definition of circuit semantics simpler and,
as the name hints, we make sure to always introduce a \emph{clocked delay} at each occurrence of \AI{DelayLoop}.
In this way we avoid \emph{combinatorial loops} in the circuit,
which can make circuit analysis significantly more complex~\cite{combinatorial-loops}.

The remaining composite constructors of \AD{ℂ} are:

\begin{itemize}
    \item Sequential composition (\AI{\_⟫\_}), which connects the output of one circuit to the input of another.
        The indices ensure that the interfaces are \emph{compatible}, i.e, that they have the same size.
    \item Parallel composition (\AI{\_∥\_}), that creates a combined circuit which has as inputs (resp. outputs)
        the inputs (resp. outputs) of \emph{both} constituent subcircuits.
\end{itemize}

The careful indexing of the sequential and parallel composition, together with the type of \AF{\_⤪\_},
ensure that some simple design mistakes are \emph{prevented by construction}.
Namely, floating wires are forbidden by \AI{\_⟫\_}: in a term ``\AB{c₁} \AI{⟫} \AB{c₂}'',
the output size of \AB{c₁} needs to equal the input size of \AB{c₂}.
Also, because \AI{Plug} takes a \emph{function} from outputs to inputs,
only one source can be assigned to each load (no short-circuits).
Lastly, the \emph{totality} of the argument to \AI{Plug} ensures that no plug output can be left unassigned.
    
In Π-Ware, circuits are parameterized both by the type of data travelling in the wires (an \ARR{Atomic} type)
and by a set of fundamental \ARR{Gates} upon which all circuits are built.
The first design choice taken when describing circuits in Π-Ware is which \ARR{Atomic} type to use,
so let's start by explaining what this choice entails.


\subsection{Atom types}
\label{subsec:atom}

    In hardware descriptions written in VHDL or Verilog, often the information carried on the wires is modelled as bits.
    This description stays close to a physical implementation and thus remains popular.
    However, sometimes it is useful to think of other types being carried in the wires:
    for example, a small enumeration type better describes the possible states of a state machine.
    In Π-Ware, all circuit descriptions are \emph{parameterized} by the type of element carried over the wires.

    Types that can be carried over circuit ports and wires are called \emph{atomic} types.
    Elements of these types are considered to have \emph{no parts} and cannot be \emph{inspected} by Π-Ware in any way.
    Some very simple examples of atomic types are: \AD{Bool}, (named) enumerations and \AD{Fin} \AB{n} (for some \AB{n}).

    Perhaps the simplest useful example of an atomic type is the often-used \AD{Bool}.
    When using the interface offered by Π-Ware, we can \emph{enumerate} the elements of \AD{Bool},
    and we can \emph{test} whether any two elements of \AD{Bool} are equal, but no other information can be extracted.

    In order to be used as an atomic type, a given type must be \emph{finite} and \emph{non-empty}.
    We pack the type itself together with these requirements in the \ARR{Atomic} record (Listing~\ref{lst:atomic}),
    therefore all circuit descriptions must be parameterized by an \emph{instance} of such record.

    \begin{listing}[ht]
        \centering{\ExecuteMetaData[code/agda/latex/PiWare/Atom.tex]{Atomic}}
        \caption{The \texttt{Atomic} record.\label{lst:atomic}}
    \end{listing}

    The first \emph{field} (\AL{Atom}) of the \ARR{Atomic} record
    is the Agda \AD{Set} denoting the type of elements carried over one wire.
    The second field (\AL{enum}) is an instance of the \ARR{FiniteNonEmpty} record
    (shown in Listing~\ref{lst:finitenonempty}).

    \begin{listing}[ht]
        \centering{\ExecuteMetaData[code/agda/latex/Data/FiniteNonEmpty/Base.tex]{FiniteNonEmpty}}
        \caption{The \texttt{FiniteNonEmpty} record.\label{lst:finitenonempty}}
    \end{listing}

    We witness the finiteness of \AL{Atom} by a bijection with \AD{Fin} \AB{n},
    and the \AL{default} field shows that the type in question has at least one inhabitant.
    The reason to forbid empty types from being used as \AL{Atom} lies in the semantics of \AI{DelayLoop}.
    
    We will cover circuit semantics with more detail in Section~\ref{sec:semantics} but, in summary,
    each occurrence of \AI{DelayLoop} prepends one extra element to the circuit's output stream.
    This extra element will have type \AD{Vec} \AL{Atom} \AB{n} (for arbitrary n),
    and thus we need to have \emph{at least one arbitrary value} of type \AL{Atom} at our disposal (\AL{default}).

    As a last remark, we make \AF{W} \AB{n} a synonym for \AD{Vec} \AL{Atom} \AB{n}.
    Thus in any context parameterized by an instance of the \ARR{Atomic} record,
    we can refer to words of atoms in a shorter and more convenient way.


\subsection{Fundamental Gates}
\label{subsec:gates}

    The \AF{mux} example from the introduction was constructed using the usual
    boolean gates (\texttt{AND}, \texttt{OR}, and \texttt{NOT}).
    Instead of hardwiring the choice of fundamental gates in the definition of circuits,
    Π-Ware allows users to choose their own collection of \emph{fundamental gates}.
    These could be the boolean gates mentioned above, but more complex circuits,
    such as muxes, registers, or arithmetic circuits could all be regarded as fundamental,
    depending on the particular design.

    To define a particular choice of fundamental gate library, users are required
    to define a suitable Agda record specifying the interface and semantics of each gate.
    This record (called \ARR{Gates}) is shown in Listing~\ref{lst:gates}.

    \begin{listing}[ht]
        \centering{\ExecuteMetaData[code/agda/latex/PiWare/Gates.tex]{Gates}}
        \caption{The \texttt{Gates} record.\label{lst:gates}} 
    \end{listing}

    The type of gate identifiers is stored in the \AL{Gate} field,
    and there are functions that assign to each gate identifier its corresponding number of inputs (\AL{|in|}),
    number of outputs (\AL{|out|}), and specification function (\AL{spec}).
    Notice the highly dependent type of \AL{spec} and of \ARR{Gates} as a whole:
    the return type of \AL{spec} depends on it's \AB{g} parameter and on the \AL{|in|} and \AL{|out|} fields.
    The type of \AL{|in|} and \AL{|out|} depends, in turn, on \AL{Gate}.

    The choice of fundamental gates strongly influences circuit
    correctness proofs: the correctness of the \ARR{Atomic} and
    \ARR{Gates} are \emph{assumed} rather than proved.

    To perform boolean logic with our circuits, we will want to use any \emph{functionally complete} set of boolean gates.
    A particularly simple such set is $\{ \text{\texttt{NAND}} \}$, which contains only the negated \texttt{AND} gate.
    First, we must define how many input and output ports does each gate in the library have:

    \begin{center}
        \ExecuteMetaData[code/agda/latex/PiWare/Gates/Nand.tex]{ins-outs}
    \end{center}

    Notice that the parameter of the \AF{|in|} and \AF{|out|} functions is of type \AD{NandGate}.
    This is the type of \emph{gate identifiers} in the library.
    There are no requirements imposed on a type to satisfy this role, but for readability we use a named enumeration.
    Having defined the interface of each gate in our library (there is only one),
    we then define the specification function:

    \begin{center}
        \ExecuteMetaData[code/agda/latex/PiWare/Gates/Nand.tex]{specs}
    \end{center}

    There are no restrictions imposed by Π-Ware on which kind of gate should or should not be present in a library,
    and higher-level \ARR{Atomic} and \ARR{Gates} instances can make designs much simpler.
    For example, with an \ARR{Atomic} instance defined to represent 8-bit signed integers,
    there can be a useful \ARR{Gates} library containing some set of modular arithmetic operators over these integers.

    As another example of gate library, Π-Ware also includes \AF{BoolTrio},
    a gate library operating over booleans with three boolean operations (\texttt{NOT}, \texttt{AND}, \texttt{OR})
    and two constant gates (\texttt{FALSE} and \texttt{TRUE}).
    We specify the behaviour of the gates using the boolean functions from Agda's standard library (\AM{Data.Bool}).

    When \emph{simulating} a Π-Ware circuit, we will use the specification functions of the gate library used in that circuit.
    Likewise, in proofs of circuit correctness, the fundamental gates are assumed to be correct.
    Therefore the elements in a \ARR{Gates} library can be seen as fundamental in two ways:

    \begin{itemize}
        \item Fundamental \emph{behaviour}, as they have no subparts.
        \item Fundamental \emph{functional correctness}, as it is assumed.
    \end{itemize}


\subsection{Abstraction levels}
\label{subsec:levels}
    Throughout this paper, we will deal with circuit models and circuit semantics only in terms of their \emph{size}
    (the \AD{ℂ} datatype is indexed by two natural numbers, representing the sizes of a circuit's input and output).
    However, Π-Ware offers a thin \emph{data abstraction} layer, allowing Agda types in circuit's inputs/outputs
    (instead of \AD{Vec} \AL{Atom}).

    A typed circuit is defined as just a wrapper record around a sized circuit (\AD{ℂ}).
    Therefore, the computation still is performed over words,
    but the description of a typed circuit contains information on how to \emph{convert}
    between elements of the involved Agda types and the correspondingly-sized words.

    This thin layer makes mainly \emph{simulation} and testing more convenient and less verbose
    (no need to always build vectors to compare with in testing, for example).
    However, as the computation is still performed over words,
    proving a circuit's correctness will still rely on lemmata involving the \emph{sized} level
    (vectors, atoms).

    We discuss with more detail in Section~\ref{sec:discussion} how this layer of data abstraction
    influences modelling and verification, and what could be other possible ways of raising the
    level of abstraction in circuit description.
