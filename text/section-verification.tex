\section{Verification}
\label{sec:verification}

\acrodef{DUT}{Design Under Test}
Π-Ware also allows for the \emph{verification} of circuit models.
The kind of properties that can be stated and verified depends on the semantics being used.
With Π-Ware in its current form, we can express mainly \emph{functional} specifications,
that is, those related to the input/output characteristics of the circuit.
Furthermore, due to the embedding in a dependently-typed language,
Π-Ware allows for both \emph{testing} of any specific circuit,
as well as \emph{proofs} of correctness for \emph{circuit generators}.

Tests and proofs can be written which check constraints on the outputs or witness arbitrary
relations between the inputs and outputs of a circuit.
In particular, the \ac{DUT} can be verified to have the same input/output behaviour
as an Agda function \emph{assumed to be correct}.
Also, we have defined a notion of \emph{extensional equivalence} between circuits,
allowing us to prove algebraic properties of circuit constructors and combinators,
as well as to define provably-correct semantics-preserving circuit transformations.


\subsection{Testing}
\label{subsec:testing}

    Testing can be used by a designer to \emph{gain confidence} in the functional correctness
    of a model early in the design process, before attempting to write proofs in their full generality.
    Writing test cases can also be a useful way to capture requirements from whoever
    commissioned the circuit, thus aiding in \emph{validation}.

    Using only the simulation functions (\AF{⟦\_⟧} and \AF{⟦\_⟧c}),
    \emph{manual} test cases can already be written: this method is usually called \emph{unit testing}.
    These are some examples of unit tests for a two-input \AF{mux}
    (the leftmost boolean in the input vector being the \emph{selection} bit):
    
    \begin{center}
        \ExecuteMetaData[code/agda/latex/PiWare/ProofSamples/Muxes.tex]{test-mux1}
        \newline
        \ExecuteMetaData[code/agda/latex/PiWare/ProofSamples/Muxes.tex]{test-mux2}
    \end{center}

    Going further then unit testing,
    the focus of this subsection is on Π-Ware's facilities to help \emph{test automation}.
    For circuits with inputs and outputs of small size, verification via \emph{exhaustive checking}
    is feasible, and our ultimate goal is to make this as automatic and concise as possible.

    The first step of abstraction from manually-written test cases is to have an Agda function
    serving as specification of the circuit behaviour.
    This means that, for any possible input to the circuit,
    evaluating the function with this input will produce an output assumed to be correct.

    Continuing with our \AF{mux} example, let's check its correctness by comparing it with a specification function.
    First of all, the simulation semantics of \AF{mux} has the following type:

    \begin{center}
      \AF{⟦} \AF{mux} \AF{⟧} \AY{:} \AF{W} \AN{3} \AY{→} \AF{W} \AN{1}
    \end{center}

    Looking at this type, and thinking about the expected behaviour of \AF{mux} (\emph{selecting} one of two inputs),
    a reasonable candidate for specification is as follows:

    \begin{center}
        \ExecuteMetaData[code/agda/latex/PiWare/ProofSamples/Muxes.tex]{ite}
    \end{center}

    The first required task for automatic exhaustive checking is to
    \emph{generate} all possible values of the circuit's input type.
    For this, we need the input type to have an instance of the \ARR{Finite} record,
    which embodies a \emph{bijection} between the type in question and \AF{Fin} \AB{n}.
    The definition of \ARR{Finite} is shown in Listing~\ref{lst:finite}.
    
    \begin{listing}
        \centering{\ExecuteMetaData[code/agda/latex/Data/Finite/Base.tex]{Finite}}
        \caption{The \texttt{Finite} record.\label{lst:finite}}
    \end{listing}

    There are some instances of \ARR{Finite} defined in the Π-Ware library for primitive types
    (amongst which \AD{Bool}), along with products, sums and vectors.
    Specifically, the input type of \AF{⟦} \AF{mux} \AF{⟧} is \AF{W} \AN{3} (equal to \AD{Vec} \AD{Bool} \AN{3}),
    so the necessary \ARR{Finite} instance relies on the pre-defined instances for vectors and booleans.

    Having a way to generate all values of a type, we can create a vector containing all of them.
    More interestingly, we can create a (heterogeneous) vector containing all of the \emph{proofs}
    that each value satisfies a certain predicate.

    We use the fact that a type is \ARR{Finite} to define an ∀-introduction rule for it.
    Then, because \AL{Atom} is required to be \ARR{Finite}, and because \AD{Vec} \AB{α} \AB{n}
    is also \ARR{Finite} (for any finite \AB{α} and any n), we define ∀-introduction for \emph{words}:

    \begin{center}
        \ExecuteMetaData[code/agda/latex/PiWare/Atom.tex]{forall-W-type}
    \end{center}

    In particular, we can then simply use \AF{∀-W} to prove properties
    involving all possible inputs of a circuit.
    As long as the chosen property \AB{P} is \emph{decidable}, the \AB{ps}
    parameter will reduce to a nested product of units and Agda will infer it.

    The property in which we are particularly interested is whether a circuit and a given specification
    function \emph{agree} on a certain input.

    \begin{center}
        \ExecuteMetaData[code/agda/latex/PiWare/Testing.tex]{implements-at-dec}
    \end{center}

    This relation relies on a decidable equality over output words of the checked circuit (\AF{\_≟W\_}),
    and uses it to compare the result obtained by running both the circuit simulation and specification function.
    By passing this relation as the \AB{P} parameter in \AF{∀-W}, we get the function we ultimately wanted:
    \AF{check⊑}.

    \begin{center}
        \ExecuteMetaData[code/agda/latex/PiWare/Testing.tex]{check-implements}
    \end{center}
    
    This function performs automatic exhaustive checking, in order to verify that a circuit complies with
    a given specification function.
    It is feasible to use \AF{check⊑} for verification of small circuits such as mux, or for small parts
    of bigger designs. However, for big designs, and to verify circuit \emph{generators},
    we need to resort to manually-written proofs.


\subsection{Proofs}
\label{subsec:proofs}
\acrodef{EDSL}{Embedded Domain-Specific Language}

    The key advantage brought to verification by using a dependently-typed language
    is that properties can be proven not only of any \emph{specific} circuit, but of \emph{circuit generators}.
    Circuit generators are \emph{parameterized} definitions from which for each value of the parameter,
    a different circuit can be derived.
    
    The term ``circuit generator'' itself comes from the Lava~\cite{lava} \acs{EDSL},
    but the idea of parameterized definitions is \emph{at least} as old as VHDL's \emph{generics}~\cite{vhdl-standard}.
    The parameters of these circuit generators are usually structural properties of the circuit,
    such as the \emph{sizes} or amount of inputs and outputs of a circuit.
    Another example would be configuring how many clock cycles does the input get delayed in
    a shift register.

    Usually, these definitions will be \emph{recursive},
    and thus the proofs of statements involving these generators will then be performed by induction.
    A circuit generator \AF{muxN}, that selects between \emph{two} inputs of size \AB{n} each,
    has the following type and definition:

    \begin{center}
        \ExecuteMetaData[code/agda/latex/PiWare/Samples/Muxes.tex]{muxN}
    \end{center}

    For a given value of the parameter \AB{n},
    this definition produces a circuit with input size \AN{1} \AO{+} \AY{(}\AB{n} \AO{+} \AB{n}\AY{)}
    (\AN{1} selection bit, plus \AB{n} bits for each input) and output size \AB{n}.
    The base case is a circuit with one input and \AI{zero} outputs,
    and that matches the size of the empty plug (\AF{nil⤨}).
    In the recursive case, we connect the 2-input \AF{mux} and the recursive call (\AF{muxN} \AB{n}) in parallel,
    and we need a \AI{Plug} (called \AF{adapt⤨}) to make the right wires meet the right ports.
    By the type of \AF{adapt⤨} it should be clear how it performs this routing:

    \begin{center}
        \ExecuteMetaData[code/agda/latex/PiWare/Samples/Muxes.tex]{adapt-type}
    \end{center}

    The first 3 bits in the output size of \AF{adapt⤨} (\AN{1} \AO{+} \AN{1} \AO{+} \AN{1})
    are exactly those needed by the \AF{mux},
    while the remaining ones are consumed by \AF{muxN} \AB{n}.

    As already mentioned before, the choice of specification function has a significant impact
    on the proof of correctness for a circuit. In the case of \AF{muxN}, the specification is \AF{iteN}:

    \begin{center}
        \ExecuteMetaData[code/agda/latex/PiWare/ProofSamples/Muxes.tex]{iteN}
    \end{center}

    In \AF{iteN}, the tail of the input is split into two equal parts and we use \AF{if\_then\_else\_}
    to choose (based on the selection bit), which of the two parts will be the output.
    Then the proof that \AF{muxN} complies with its specification will need to follow the same induction
    pattern used to define the specification itself. Namely, we need to pattern match on \AB{n}
    and do a case analysis on the result of splitting the tail of the input.

    \begin{center}
        \ExecuteMetaData[code/agda/latex/PiWare/ProofSamples/Muxes.tex]{muxN-impl-iteN}
    \end{center}

    Unfortunately, the full proof of \AF{muxN⊑iteN} is a bit too long to be completely
    analyzed here (we abbreviate at \AF{muxN⊑iteN′}). The proof relies of course on the proof
    of correctness for \AF{mux} (the basic circuit with type \AD{ℂ} \AN{3} \AN{1}).
    It also relies on properties of the \AF{adapt⤨} plug, ensuring that it's semantics
    essentially commutes and associates the arguments of functions in the necessary way.


\subsection{Connection patterns}
\label{subsec:connection-patterns}

    We are interested not only in proving properties of circuits in isolation,
    but also about the behaviour of so-called
    \emph{connection patterns}\footnote{This name comes from Lava as well.}.
    Connection patterns are just functions taking circuits as inputs and producing circuits as outputs.
    Typically they use the constructors of \AD{ℂ} to \emph{connect} their arguments in a certain fashion,
    thus the name.

    A (very simple) example of connection pattern is \AF{parsN},
    which connects \AB{n} copies of a given circuit in parallel.
    The type and definition of \AF{parsN} are:

    \begin{center}
        \ExecuteMetaData[code/agda/latex/PiWare/Patterns.tex]{parsN}
    \end{center}

    Notice how the input and output sizes of the combined circuit are \emph{statically guaranteed}
    to be correct, as they are calculated from the input/output sizes of the circuit passed as parameter.
    This definition (\AF{parsN}) is a special case of a more general pattern:
    instead of \AF{replicat}ing the same circuit \AB{n} times,
    we can connect a whole vector of (different) circuits in parallel. This is achieved by \AF{pars}:

    \begin{center}
        \ExecuteMetaData[code/agda/latex/PiWare/Patterns.tex]{pars}
    \end{center}

    As the parameter of \AF{pars} we need a special kind of vector, ensuring that only elements of types
    built with a given type constructor (\AD{ℂ}) can be present in the vector.
    This special kind of vector is \AD{VecI₂}, what we call
    a \emph{index-heterogeneous vector}~\footnote{More specifically, \AD{VecI₂} only handles type constructors with \emph{two} indices.}.
    It is heterogeneous in the sense that its elements have different types,
    but only the indices vary, and the type constructor is fixed for all elements.

    Another case of basic connection pattern is \AF{seqsN},
    taking a circuit and connecting \AB{n} copies of it in sequence:

    \begin{center}
        \ExecuteMetaData[code/agda/latex/PiWare/Patterns.tex]{seqsN}
    \end{center}

    Notice how the input and output sizes of the argument circuit are the same (\AB{io}).
    This is because the \AI{\_⟫\_} constructor forces
    the output size of a circuit in this sequence to match the input size of the next.

    Also \AF{seqsN} is a special case of a general pattern:
    connecting a vector with \AB{n} circuits in sequence.
    For this connection to be even \emph{possible}, we need the input/output sizes of the
    circuits in the vector to be \emph{pairwise compatible}.
    This means that for each circuit, its output size must be equal to the input size of the next.
    To this end, we adapt the work done
    on \emph{type-aligned sequences}~\cite{ploeg} to a dependently-typed setting.

    We are currently working in establishing lemmata about the behaviour of these connection patterns
    in order to make proofs involving their usage simpler.
    For example, the simulation behaviour of \AF{seqsN} can be shown to be that of the \AF{iterate} function,
    that is:

\begin{center}
\AY{∀} \AB{w} \AY{→} \AF{⟦} \AF{seqsN} \AB{k} \AB{c} \AF{⟧} \AB{w} \AD{≡} \AY{(}\AF{iterate} \AB{k} \AF{⟦} \AB{c} \AF{⟧}\AY{)} \AB{w}
\end{center}

    Furthermore, we are also working on expressing connection patterns as folds over the underlying
    (indexed heterogeneous) vectors, as that would allow for more general and powerful laws.


\subsection{Circuit equivalence}
\label{subsec:circuit-equivalence}

    Until now we have talked about relations between a circuit and a function — such
    as the \emph{complies with} relation (i.e. ``\AB{c} \AF{⊑} \AB{f}'').
    However, it is also very important to have an \emph{equivalence relation} between circuits themselves.
    Given a properly-defined such relation, we can then have at our disposal laws like
    ``\AB{c} \AI{⟫} \AF{id⤨} \AD{≋} \AB{c}'', allowing for \emph{provably safe} circuit optimizations.

    We will explain in this section the several iterations that we have gone through until achieving the
    current definition of circuit equivalence, correcting a small issue at each step.
    In the most naïve and first attempt, we just require identical inputs and compare the outputs of
    simulating both circuits using (propositional) equality.

    \begin{center}
        \ExecuteMetaData[code/agda/latex/PiWare/Simulation/Equality.tex]{Circ-eq-sim-one-input-prop}
    \end{center}

    This definition is very unsatisfactory though, because it can only be used to compare circuits with
    \emph{definitionally equal} indices,
    i.e, we cannot compare \AY{(}\AB{c₁} \AY{:} \AD{ℂ} \AN{1} \AB{n}\AY{)} with \AY{(}\AB{c₂} \AY{:} \AD{ℂ} \AN{1} \AY{(}\AB{n} \AO{+} \AN{0}\AY{)}\AY{)}.
    The first improvement over this definition is to use \emph{vector equality} to compare the outputs.
    The notion of \emph{semi-heterogeneous} vector equality (\AD{\_≈\_}) is defined in Agda's standard library and
    it considers two vectors equal whenever the elements are \emph{pointwise} definitionally equal.
    The new definition of circuit equivalence looks as follows:

    \begin{center}
        \ExecuteMetaData[code/agda/latex/PiWare/Simulation/Equality.tex]{Circ-eq-sim-one-input}
    \end{center}

    While now the problem of word size (\AB{n} \AO{+} \AN{0} vs. \AB{n}) has been solved for the outputs,
    the same issue remains for the input:
    we cannot yet compare \AY{(}\AB{c₁} \AY{:} \AD{ℂ} \AB{n} \AN{1}\AY{)} with \AY{(}\AB{c₂} \AY{:} \AD{ℂ} \AY{(}\AB{n} \AO{+} \AN{0}\AY{)} \AN{1}\AY{)}.
    Ultimately, what we want for circuit equivalence is to ensure that, when given ``vector equal'' inputs,
    both circuits will generate ``vector equal'' outputs:

    \begin{center}
        \ExecuteMetaData[code/agda/latex/PiWare/Simulation/Equality.tex]{Circ-eq-sim-equal-inputs}
    \end{center}

    This is the definition that \emph{almost} gets us there. It has a big problem, though: it's unsound.
    Very easily we can construct a term of type \AY{(}\AB{c₁} \AF{≊} \AB{c₂}\AY{)} simply by making sure the hypothesis is false.
    A simple example of a term that should be banned by \AF{\_≊\_} but is allowed is the following:

    \begin{center}
        \ExecuteMetaData[code/agda/latex/PiWare/Simulation/Equality.tex]{Circ-eq-sim-equal-inputs-unsound}
    \end{center}

    To solve this issue, we make an extra requirement for two circuits to be considered equal.
    Now, not only vector equal inputs must lead to vector equal outputs, but also
    there must be a proof that the sizes of the input words are propositionally equal.

    \begin{center}
        \ExecuteMetaData[code/agda/latex/PiWare/Simulation/Equality.tex]{Circ-eq-sim}
    \end{center}

    This is the definition of circuit equivalence that we use, to state algebraic properties of circuit
    constructors and combinators, and also in the case study discussed in Section~\ref{sec:casestudy}.
    For extra convenience, we packed up \AY{(}\AD{ℂ}, \AF{\_≋\_}\AY{)} into an indexed setoid structure,
    and we added to Agda's standard library some facilities for \emph{equational reasoning} with indexed setoids.
    All in all, this allows proofs about circuit equivalence to be written in a very nice-looking style.
    A good example of such a proof can be seen in Listing~\ref{lst:serial-is-scan} of
    Section~\ref{sec:casestudy}.
